package com.PropostaCaseCartoes.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @OneToOne
    private Cartao cartao;
    @NotNull
    private String descricao;
    @NotNull
    private double valor;

    public Pagamento() {
    }

    public Pagamento(int id, @NotNull Cartao cartao, @NotNull String descricao, @NotNull double valor) {
        this.id = id;
        this.cartao = cartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
