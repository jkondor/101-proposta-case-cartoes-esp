package com.PropostaCaseCartoes.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull(message = "Nome do cartao deve ser preenchido.")
    private String numero;
    @NotNull
    @ManyToOne
    private Cliente cliente;
    @NotNull
    private boolean ativo;

    public Cartao() {
    }

    public Cartao(int id, @NotNull(message = "Nome do cartao deve ser preenchido.") String numero, Cliente cliente, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.cliente = cliente;
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
