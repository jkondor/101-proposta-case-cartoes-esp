package com.PropostaCaseCartoes.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull(message = "Nome do cliente deve ser preenchido.")
    private String name;

    public Cliente() {
    }

    public Cliente(int id, @NotNull(message = "Nome do cliente deve ser preenchido.") String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
