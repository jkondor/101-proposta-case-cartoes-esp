package com.PropostaCaseCartoes.models.dto;

public class SituacaoCartaoDTO {

    private boolean ativo;

    public SituacaoCartaoDTO() {
    }

    public SituacaoCartaoDTO(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
