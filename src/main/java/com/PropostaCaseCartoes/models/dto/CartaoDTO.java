package com.PropostaCaseCartoes.models.dto;

public class CartaoDTO {

    private String numero;
    private int clienteId;

    public CartaoDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
