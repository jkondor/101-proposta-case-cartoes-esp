package com.PropostaCaseCartoes.models.dto;

import com.PropostaCaseCartoes.models.Cartao;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

public class PagamentoDTO {

    private int id;
    private int cartaoid;
    private String descricao;
    private double valor;

    public PagamentoDTO() {
    }

    public PagamentoDTO(int id, int cartaoid, String descricao, double valor) {
        this.id = id;
        this.cartaoid = cartaoid;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartaoid() {
        return cartaoid;
    }

    public void setCartaoid(int cartaoid) {
        this.cartaoid = cartaoid;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
