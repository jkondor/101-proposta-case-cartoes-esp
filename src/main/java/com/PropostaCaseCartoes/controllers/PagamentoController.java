package com.PropostaCaseCartoes.controllers;

import com.PropostaCaseCartoes.models.Cartao;
import com.PropostaCaseCartoes.models.Pagamento;
import com.PropostaCaseCartoes.models.dto.PagamentoDTO;
import com.PropostaCaseCartoes.models.dto.PagamentoRequestDTO;
import com.PropostaCaseCartoes.services.CartaoService;
import com.PropostaCaseCartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public PagamentoDTO incluirPagamento(@RequestBody PagamentoRequestDTO pagamentoRequestDTO){

        Optional<Cartao> cartaoOptional= cartaoService.buscarCartaoPorId(pagamentoRequestDTO.getCartao_id());
        if (cartaoOptional.isPresent()){
            Pagamento pagamento = new Pagamento();
            pagamento.setCartao(cartaoOptional.get());
            pagamento.setDescricao(pagamentoRequestDTO.getDescricao());
            pagamento.setValor(pagamentoRequestDTO.getValor());

            Pagamento pagamentoObj  = pagamentoService.incluirPagamento(pagamento);
            PagamentoDTO pagamentoDTOObj = new PagamentoDTO();
            pagamentoDTOObj.setCartaoid(pagamentoObj.getCartao().getId());
            pagamentoDTOObj.setDescricao(pagamentoObj.getDescricao());
            pagamentoDTOObj.setId(pagamentoObj.getId());
            pagamentoDTOObj.setValor(pagamentoObj.getValor());
            return pagamentoDTOObj;

        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{cartaoId}")
    public List<PagamentoDTO> buscarPagamentosCartao(@PathVariable int cartaoId){
        List<PagamentoDTO> extratoPagamentoDTOList = new ArrayList<>();
            Optional<Cartao> cartaoOptional= cartaoService.buscarCartaoPorId(cartaoId);
        if (cartaoOptional.isPresent()) {
            Iterable<Pagamento> pagamentoIterable = pagamentoService.buscarExtrato(cartaoOptional.get());

            for (Pagamento pagamento: pagamentoIterable ){
                PagamentoDTO pagamentoDTO = new PagamentoDTO(pagamento.getId(),pagamento.getCartao().getId(),
                                                             pagamento.getDescricao(), pagamento.getValor());
                extratoPagamentoDTOList.add(pagamentoDTO);
            }

            return  extratoPagamentoDTOList;
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }


}
