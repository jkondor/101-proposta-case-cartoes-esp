package com.PropostaCaseCartoes.controllers;

import com.PropostaCaseCartoes.models.Cartao;
import com.PropostaCaseCartoes.models.Cliente;
import com.PropostaCaseCartoes.models.dto.CartaoDTO;
import com.PropostaCaseCartoes.models.dto.CartaoResponseDTO;
import com.PropostaCaseCartoes.models.dto.CartaoResponseGetDTO;
import com.PropostaCaseCartoes.models.dto.SituacaoCartaoDTO;
import com.PropostaCaseCartoes.services.CartaoService;
import com.PropostaCaseCartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;
    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<CartaoResponseDTO> incluirCartao(@RequestBody @Valid CartaoDTO cartaoDTO){

        Optional<Cliente> clienteOptional = clienteService.buscarClientePorId(cartaoDTO.getClienteId());
        if (clienteOptional.isPresent()){
            Cartao cartao = new Cartao();
            cartao.setNumero(cartaoDTO.getNumero());
            cartao.setCliente(clienteOptional.get());
            cartao.setAtivo(false);
            Cartao cartaoObj = cartaoService.incluirCartao(cartao);

            CartaoResponseDTO cartaoResponseDTO = new CartaoResponseDTO(cartaoObj.getId(),cartaoObj.getNumero(), cartaoObj.getCliente().getId(),cartaoObj.isAtivo());

            return ResponseEntity.status(201).body(cartaoResponseDTO);

        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não existe");
        }
    }

    @PatchMapping("/{numero}")
    public CartaoResponseDTO atualizarSituacaoCartao(@PathVariable String numero, @RequestBody @Valid SituacaoCartaoDTO ativo){

        Cartao cartaoObj = cartaoService.atualuzarSituacaoCartao(numero, ativo.isAtivo());
        CartaoResponseDTO cartaoResponseDTO = new CartaoResponseDTO(cartaoObj.getId(),cartaoObj.getNumero(), cartaoObj.getCliente().getId(),cartaoObj.isAtivo());
        return cartaoResponseDTO;
    }

    @GetMapping("/{numeroCartao}")
    public CartaoResponseGetDTO buscarCartaoPorNumero(@PathVariable String numeroCartao){

        Optional<Cartao> cartaoOptional = cartaoService.buscarCartaoPorNumero(numeroCartao);

        if (cartaoOptional.isPresent()) {
            CartaoResponseGetDTO cartaoResponseGetDTO = new CartaoResponseGetDTO(cartaoOptional.get().getId(),
                                                                                 cartaoOptional.get().getNumero(),
                                                                                 cartaoOptional.get().getCliente().getId());

            return cartaoResponseGetDTO;
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }


}
