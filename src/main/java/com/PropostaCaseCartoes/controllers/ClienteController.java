package com.PropostaCaseCartoes.controllers;

import com.PropostaCaseCartoes.models.Cliente;
import com.PropostaCaseCartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> incluirCliente(@RequestBody @Valid Cliente cliente){

        Cliente clienteObj = clienteService.incluirCliente(cliente);
        return ResponseEntity.status(201).body(clienteObj);
    }

    @GetMapping("/{id}")
    public Cliente buscarClienteporId(@PathVariable Integer id){

        Optional<Cliente> clienteOptional = clienteService.buscarClientePorId(id);

        if (clienteOptional.isPresent()) {
            return clienteOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

}
