package com.PropostaCaseCartoes.services;

import com.PropostaCaseCartoes.models.Cliente;
import com.PropostaCaseCartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente incluirCliente(Cliente cliente){
        Cliente clienteObj = clienteRepository.save(cliente);
        return clienteObj;
    }

    public Optional<Cliente> buscarClientePorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        return  clienteOptional;
    }

}
