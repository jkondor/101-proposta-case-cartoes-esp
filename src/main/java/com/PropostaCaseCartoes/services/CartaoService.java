package com.PropostaCaseCartoes.services;

import com.PropostaCaseCartoes.models.Cartao;
import com.PropostaCaseCartoes.models.Cliente;
import com.PropostaCaseCartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao incluirCartao(Cartao cartao){
        Cartao cartaoObj = cartaoRepository.save(cartao);
        return cartaoObj;
    }

    public Optional<Cartao> buscarCartaoPorNumero(String numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        return cartaoOptional;
    }

    public Optional<Cartao> buscarCartaoPorId(int id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        return  cartaoOptional;
    }

    public Cartao atualuzarSituacaoCartao(String numero, Boolean situacao){
        Cartao cartaoObj;

        Optional<Cartao> cartaoOptional = buscarCartaoPorNumero(numero);
        if (cartaoOptional.isPresent()){
            Cartao cartaoSolicitado = cartaoOptional.get();
            cartaoSolicitado.setAtivo(situacao);
            cartaoObj = cartaoRepository.save(cartaoSolicitado);

        }else {
            cartaoObj = new Cartao();
        }
        return cartaoObj;
    }



}
