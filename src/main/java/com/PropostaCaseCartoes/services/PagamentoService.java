package com.PropostaCaseCartoes.services;

import com.PropostaCaseCartoes.models.Cartao;
import com.PropostaCaseCartoes.models.Pagamento;
import com.PropostaCaseCartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    public Pagamento incluirPagamento(Pagamento pagamento){
        Pagamento pagamentoObj = pagamentoRepository.save(pagamento);
        return pagamentoObj;
    }

    public Iterable<Pagamento> buscarExtrato(Cartao cartao){
        Iterable<Pagamento> pagamentoIterable = pagamentoRepository.findByCartao(cartao);
        return pagamentoIterable;
    }
}
