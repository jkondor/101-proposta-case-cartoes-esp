package com.PropostaCaseCartoes.repositories;

import ch.qos.logback.core.net.server.Client;
import com.PropostaCaseCartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {

}
