package com.PropostaCaseCartoes.repositories;

import com.PropostaCaseCartoes.models.Cartao;
import com.PropostaCaseCartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;


public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    Iterable<Pagamento> findByCartao(Cartao cartao);

}
